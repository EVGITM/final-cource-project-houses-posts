import { Injectable } from '@angular/core';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { finalize, flatMap, last, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UploadService {
  file;
  task: AngularFireUploadTask;

  percentage: Observable<number>;
  snapshot: Observable<any>;
  downloadURL: Observable<string>;
  URL:string;

  constructor(private storage: AngularFireStorage, private db: AngularFirestore) { }

  

  startUpload(imgFile ) {
    this.file = imgFile;
    // The storage path

    const path = `angular-project-jce/${Date.now()}_${this.file.name}`;

    // Reference to storage bucket
    const ref = this.storage.ref(path);

    // The main task
    this.task = this.storage.upload(path, this.file);

    // Progress monitoring
    this.percentage = this.task.percentageChanges();

    return <Observable<string>> this.task.snapshotChanges().pipe(
      last(),
      flatMap(
        () => {
          this.downloadURL = ref.getDownloadURL();

          return this.downloadURL.pipe(
            map((url) => {
              return  url;
              ;
            })
          );
        }
      )
    );

    /* standart function when no need to component return
    
     this.snapshot = this.task.snapshotChanges().pipe(
      finalize( ()=>{
        console.log("percent finalize");
        
        this.downloadURL = ref.getDownloadURL();
        this.downloadURL.subscribe(url => {
          if (url) {
            this.URL = url;
          }
          console.log(this.URL);
          this.db.doc(`posts/HZDneOmHIkWUBOA5kt4J/`).update( { imgURL: this.URL, path });

        });

        })

      ); and need to subscribe
      */
      
    }
}

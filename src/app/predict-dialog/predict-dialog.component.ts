import { Component, Inject, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  prediction:number;
  isYesClicked:boolean;
}

@Component({
  selector: 'app-predict-dialog',
  templateUrl: './predict-dialog.component.html',
  styleUrls: ['./predict-dialog.component.css']
})
export class PredictDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<PredictDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }



  onNoClickNo(): void {
    this.dialogRef.close();
  }

  onNoClickYes(): void {
    this.data.isYesClicked = true;
    this.dialogRef.close(this.data.isYesClicked);
  }
  
  ngOnInit(): void {
  }

}

import { Post } from '../interfaces/post';
import { Component, OnInit,Input,Output, EventEmitter, Inject} from '@angular/core';
import { AuthService } from '../auth.service';
import { PostsService } from '../posts.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import { UploadService } from '../upload.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { PredictDialogComponent } from '../predict-dialog/predict-dialog.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  prediction:number;
  isYesClicked:boolean;
}

@Component({
  selector: 'post-form-edit',
  templateUrl: './post-form-edit.component.html',
  styleUrls: ['./post-form-edit.component.css'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {showError: true}
  }]

})
export class PostFormComponentEdit implements OnInit {

  constructor (private postsService:PostsService, 
               public Auth:AuthService,
               private _formBuilder: FormBuilder,
               private UploadService:UploadService,
               public router:Router,
               public dialog: MatDialog) { }


  // Form Groups for 'Form Stepper'
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;

  userId:string;
//   ------------  Variables ---------
  @Input() postId:string;
  @Input() propertType:string;
  // for Google maps view and MARK --
  @Input() city:string;
  @Input() street:string;
  @Input() lat:number = 31.771959;;
  @Input() lng:number = 35.217018;
  //--
  @Input() yearBuilt:number;
  @Input() sqf:number;
  @Input() bedrooms:number;
  @Input() bathrooms:number;
  @Input() floors:number;
  @Input() hallwayType:string;
  @Input() heatingType:string;
  @Input() managmntType:string;
  @Input() N_Parkinglot:number;
  @Input() timeToPublicTransport:string;
  @Input() parks:number;
  @Input() elementry:number;
  @Input() university:number;
  @Input() sellPrice:number;
  @Input() imageURL:string;
  @Input() EstimatedPrice:number;
  // google map zoom
  zoom: number= 15;
  // vars for google places Adress 
  @Input() formattedAdrress = '';
  formattedAdrressArray:any;
  options= {
    types: [],
    //componentRestrictions: { country: 'IL' }
    }

  // NgModule Inputs Options Arrays
  propertTypes = ['Single-family','Multi-family', 'Condo'];
  years = [];
  hallwayTypes = ['corridor','terraced', 'mixed'];
  heatingTypes = ['individual heating','central'];
  managmntTypes = ['management in trust','self management'];
  timesToPublicTransport = ['0~5min','5min~10min', '10min~15min'];
  
  // Image Input and Upload
  image: File;
  URL$:Observable<any>;
  spinnerStatus:boolean = false;

  // Predict PopUp
  prediction:number;

  @Output() closeEdit = new EventEmitter<null>();


//   ------------  Functions   ---------
tellNavToClose(){

  this.closeEdit.emit(); 
  
}

deletePost(){
  this.postsService.deletePost(this.postId);
  this.tellNavToClose();

}
  add(){ 
    this.postsService.addPost(this.propertType,  this.formattedAdrress, this.city, this.street, this.lat, this.lng,  
    this.yearBuilt, this.sqf, this.bedrooms, this.bathrooms, 
    this.floors, this.hallwayType, this.heatingType, this.managmntType, this.N_Parkinglot, 
    this.timeToPublicTransport, this.parks, 
    this.elementry, this.university,this.sellPrice, this.imageURL,this.userId);
    this.router.navigate(['my-posts']); 

  }

  update(){
    this.postsService.updatePost(this.postId,this.propertType,  this.formattedAdrress, this.city, this.street, this.lat, this.lng,  
      this.yearBuilt, this.sqf, this.bedrooms, this.bathrooms, 
      this.floors, this.hallwayType, this.heatingType, this.managmntType, this.N_Parkinglot, 
      this.timeToPublicTransport, this.parks, 
      this.elementry, this.university,this.sellPrice, this.imageURL,this.userId);
      this.tellNavToClose();
  }

  
// image upload
uploadFileEvt(imgFile) {
  this.image = imgFile.target.files[0];
  if(this.image!= undefined){
    console.log(this.image)
    this.imageURL = undefined;
    this.spinnerStatus = true;
    this.UploadService.startUpload(this.image).subscribe(
    url => {
        this.imageURL= url
        console.log(this.imageURL);
        this.spinnerStatus = false;
    }
  );}
}


  // capture address object and extract from formattedAdrress - city, street
  public handleAddressChange(address: any) {
    console.log(address);

    this.zoom = 15;
    this.lng = address.geometry.location.lng();
    this.lat = address.geometry.location.lat();
    this.formattedAdrress = address.formatted_address;
    console.log(this.formattedAdrress);
    
    this.formattedAdrressArray  = this.formattedAdrress.split(", ");
    this.city = this.formattedAdrressArray[1]; 
    this.street = this.formattedAdrressArray[0]; 
    this.street = this.street.replace("St","");

}



  ngOnInit(): void {
    this.Auth.getUser().subscribe(
      (user) =>{
        this.userId = user.uid;
        console.log(this.userId);})

    // years list options
    for(var i = 2021; i >= 1920; i--){
      this.years.push(i);



    // stepper forms validation by input's 'formControlName'
    this.firstFormGroup = this._formBuilder.group({  
        address: ['', Validators.required],
        propertType: [null, Validators.required]
   
    });

    this.secondFormGroup = this._formBuilder.group({
      yearBuilt: ['', Validators.required],
      sqf: ['', Validators.required],
      floors: ['', Validators.required],
      bedrooms: ['', Validators.required],
      bathrooms: ['', Validators.required],

    });

    this.thirdFormGroup = this._formBuilder.group({
      heatingType: ['', Validators.required],
      hallwayType: ['', Validators.required],
      managmntType: ['', Validators.required],
      N_Parkinglot: ['', Validators.required],
      timeToPublicTransport: ['', Validators.required],
      parks: ['', Validators.required],
      elementry: ['', Validators.required],
      university: ['', Validators.required],

    });

    this.fourthFormGroup = this._formBuilder.group({  
      // image: ['', Validators.required],

   });

    this.fifthFormGroup = this._formBuilder.group({  
      sellPrice: ['', Validators.required],
 
  });
   



  }
}

predict(){

  this.postsService.predict(this.yearBuilt,this.sqf,this.floors,this.hallwayType,this.heatingType,this.managmntType,this.N_Parkinglot,this.timeToPublicTransport,this.parks,this.elementry,this.university).subscribe(
    (res) => {
      console.log(res)
      this.prediction = Math.round(Number(res));

      const dialogRef = this.dialog.open(PredictDialogComponent, {
        width: '500px',
        data: {prediction: this.prediction, isYesClicked:false}
      });
    
      dialogRef.afterClosed().subscribe(result => {
        if (result == true){
          this.sellPrice = this.prediction;
        }
      });
     
      }
    )

  


  }

  }
  



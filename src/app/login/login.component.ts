import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  hide=true;
  email:string;
  password:string;
  hasError:boolean = false;
  errorMessage:string;

  onSubmit(){
    this.Auth.login(this.email, this.password).then(res => {
      console.log("Login Successful");
      this.router.navigate(['']); 
    }).catch((error) => {
      this.hasError=true;
      this.errorMessage = error.message;
    })
  }

  constructor(private Auth:AuthService ,public router:Router) { }
  

  
  ngOnInit(): void {
  }

}

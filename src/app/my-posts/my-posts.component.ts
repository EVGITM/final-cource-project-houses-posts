import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { AuthService } from './../auth.service';
import { PostsService } from '../posts.service';
import { Post } from '../interfaces/post';
import { Router } from '@angular/router';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

@Component({
  selector: 'my-posts',
  templateUrl: './my-posts.component.html',
  styleUrls: ['./my-posts.component.css']
})

export class MyPostsComponent implements OnInit {
  image: File;
  posts$;
  userId:string; 
  editstate:Array <boolean> = [];
  addBookFormOPen:boolean = false;
  posts:Array <Post>;

  postId:string;
  propertType:string;
  // for Google maps view and MARK --
  formattedAdrress:string;
  city:string;
  street:string;
  lat:number = 31.771959;;
  lng:number = 35.217018;
  //--
  yearBuilt:number;
  sqf:number;
  bedrooms:number;
  bathrooms:number;
  floors:number;
  hallwayType:string;
  heatingType:string;
  managmntType:string;
  N_Parkinglot:number;
  timeToPublicTransport:string;
  parks:number;
  elementry:number;
  university:number;
  sellPrice:number;
  imageURL:string;
  EstimatedPrice:number;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  constructor(private breakpointObserver: BreakpointObserver, private postsService:PostsService, public Auth:AuthService,public router:Router) { }

  TurnToEditState(postId, index){
    this.postId=this.posts[index].id;
    this.propertType=this.posts[index].propertType;
    this.formattedAdrress=this.posts[index].formattedAdrress;
    this.city=this.posts[index].city;
    this.street=this.posts[index].street;
    this.lat=this.posts[index].lat;
    this.lng=this.posts[index].lng;
    this.yearBuilt=this.posts[index].yearBuilt;
    this.sqf=this.posts[index].sqf;
    this.bedrooms=this.posts[index].bedrooms;
    this.bathrooms=this.posts[index].bathrooms;
    this.floors=this.posts[index].floors;
    this.hallwayType=this.posts[index].hallwayType;
    this.heatingType=this.posts[index].heatingType;
    this.managmntType=this.posts[index].managmntType;
    this.N_Parkinglot=this.posts[index].N_Parkinglot;
    this.timeToPublicTransport=this.posts[index].timeToPublicTransport;
    this.parks=this.posts[index].parks;
    this.elementry=this.posts[index].elementry;
    this.university=this.posts[index].university;
    this.sellPrice=this.posts[index].sellPrice;
    this.imageURL=this.posts[index].imageURL;
  
  }

  test(){
    console.log("b");
  }


  
  
  ngOnInit(): void {
    this.Auth.getUser().subscribe(
      (user) =>{
        this.userId = user.uid;
        console.log("Posts Of UserID:");
        console.log(this.userId);
        
    this.posts$ = this.postsService.getMyPosts(this.userId);
    this.posts$.subscribe(
     docs =>{
       this.posts = [];
       for(let document of docs){
         const post:Post = document.payload.doc.data();
         post.id = document.payload.doc.id; 

         this.posts.push(post); 
       }
       console.log(this.posts);

   
 }
)
})
}

}

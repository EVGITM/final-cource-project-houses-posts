import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class PostsService {
  postsCollection:AngularFirestoreCollection;
  private URL = "https://ri3qwl6g5i.execute-api.us-east-1.amazonaws.com/examA-evgeni";

  constructor(private db: AngularFirestore, private http:HttpClient) { }


  public getAllPosts(){
    this.postsCollection = this.db.collection(`posts/`, 
    ref => ref.orderBy('updated_at', 'desc'));     
    return this.postsCollection.snapshotChanges();
   }

   public getMyPosts(userId){
    this.postsCollection = this.db.collection(`posts/`, 
    ref => ref.where("userId", "==", userId));     
    return this.postsCollection.snapshotChanges();
   }

   public getPosttoEdit(PostId){
     console.log('mnjkdsnfs')
    console.log(this.db.collection("posts").doc(PostId)); 


   }

   deletePost( PostId:string ){
    this.db.doc(`posts/${PostId}`).delete();
  
  }

   addPost(propertType, formattedAdrress, city, street, lat, lng,  
    yearBuilt, sqf, bedrooms, bathrooms, 
    floors, hallwayType, heatingType, managmntType, N_Parkinglot, 
    timeToPublicTransport, parks, 
    elementry, university,sellPrice, imageURL,userId){

    const post  = {propertType:propertType, formattedAdrress:formattedAdrress, city:city, street:street, lat:lat, lng:lng,  
      yearBuilt:yearBuilt, sqf:sqf, bedrooms:bedrooms, bathrooms:bathrooms, 
      floors:floors, hallwayType:hallwayType, heatingType:heatingType, managmntType:managmntType, N_Parkinglot:N_Parkinglot, 
      timeToPublicTransport:timeToPublicTransport, parks:parks, 
      elementry:elementry, university:university, sellPrice:sellPrice, imageURL:imageURL,userId:userId, updated_at: Date.now()} 

      this.db.collection(`posts`).add(post).then((res)=> {   console.log("Document written with ID: ", res.id)}
    );
  }

  updatePost(postId,propertType, formattedAdrress, city, street, lat, lng,  
    yearBuilt, sqf, bedrooms, bathrooms, 
    floors, hallwayType, heatingType, managmntType, N_Parkinglot, 
    timeToPublicTransport, parks, 
    elementry, university,sellPrice, imageURL,userId){

    const post  = {propertType:propertType, formattedAdrress:formattedAdrress, city:city, street:street, lat:lat, lng:lng,  
      yearBuilt:yearBuilt, sqf:sqf, bedrooms:bedrooms, bathrooms:bathrooms, 
      floors:floors, hallwayType:hallwayType, heatingType:heatingType, managmntType:managmntType, N_Parkinglot:N_Parkinglot, 
      timeToPublicTransport:timeToPublicTransport, parks:parks, 
      elementry:elementry, university:university, sellPrice:sellPrice, imageURL:imageURL,userId:userId, updated_at: Date.now()} 

      this.db.doc(`posts/${postId}`).update(post)
  }

  predict(yearBuilt:number, sqf:number, floors:number, hallwayType:string, heatingType:string,
    managmntType:string,N_Parkinglot:number, timeToPublicTransport:string, parks:number, elementry:number, university:number){
  
    let json = {"data": `${yearBuilt},${sqf},${floors},${hallwayType},${heatingType},${managmntType},${N_Parkinglot},${timeToPublicTransport},${parks},${elementry},${university}`
   }
   console.log("JSON To Be Sent To Lmbda:")
   console.log(json)
    let body = JSON.stringify(json);
    return this.http.post<any>(this.URL, body).pipe(map(res =>{
      console.log(res);
      let result:string = res.result;     
      return result;

    })
    )

  }






}

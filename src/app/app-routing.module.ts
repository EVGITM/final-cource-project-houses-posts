import { SignupComponent } from './signup/signup.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { PostsComponent } from './posts/posts.component';
import { PostFormComponent } from './post-form/post-form.component';
import { MyPostsComponent } from './my-posts/my-posts.component';

const routes: Routes = [  

  { path: 'login', component: LoginComponent },  
  { path: 'signup', component: SignupComponent },  
  { path: '', component: PostsComponent },  
  { path: 'post-form', component: PostFormComponent },  
  { path: 'my-posts', component: MyPostsComponent },  



  
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from './../auth.service';
import { PostsService } from '../posts.service';
import { Post } from '../interfaces/post';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
    //functions:

  posts$;
  posts:Array <Post>

  constructor(private postsService:PostsService, public Auth:AuthService) { }

  //functions:

  // Get All Posts
  ngOnInit(): void {
        this.posts$ = this.postsService.getAllPosts();
        this.posts$.subscribe(
         docs =>{
           this.posts = [];
           for(let document of docs){
             const post:Post = document.payload.doc.data();
             post.id = document.payload.doc.id; 
             this.posts.push(post); 
            } 
            console.log("Posts Loaded:");
            console.log(this.posts);          }
        )
      }
    
 
 
}
 
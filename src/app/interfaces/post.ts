export interface Post {
    id: string ,
    propertType:string,
    formattedAdrress:string,
    city:string,
    street:string,
    lat:number,
    lng:number,
    yearBuilt:number,
    sqf:number,
    bedrooms:number,
    bathrooms:number,
    floors:number,
    hallwayType:string,
    heatingType:string,
    managmntType:string,
    N_Parkinglot:number,
    timeToPublicTransport:string,
    parks:number,
    elementry:number,
    university:number,
    sellPrice:number,
    imageURL:string,
    user_id:string

 
}
